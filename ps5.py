# 6.00.2x Problem Set 5
# Graph optimization
# Finding shortest paths through MIT buildings
#

import string
# This imports everything from `graph.py` as if it was defined in this file!
from graph import *

#
# Problem 2: Building up the Campus Map
#
# Before you write any code, write a couple of sentences here
# describing how you will model this problem as a graph.

# This is a helpful exercise to help you organize your
# thoughts before you tackle a big design problem!
#

def load_map(mapFilename):
  print "Loading map from file..."
  with open(mapFilename) as f:
    lines = f.readlines()

  g = WeightedDigraph()

  def addNodeIfHasNo(g, node):
    if not g.hasNode(node):
      g.addNode(node)


  for l in lines:
    src, dstn, dist, outdist = l.split(' ')
    n1 = Node(src)
    n2 = Node(dstn)
    addNodeIfHasNo(g, n1)
    addNodeIfHasNo(g, n2)
    g.addEdge(WeightedEdge(n1, n2, int(dist), int(outdist)))

  return g

#
# Problem 3: Finding the Shortest Path using Brute Force Search
#
# State the optimization problem as a function to minimize
# and what the constraints are
#

def listOfTuples(l):
  return map(lambda e : tuple(e), l)

def listOfLists(l):
  return map(lambda e : list(e), l)

def uniq(l):
  return listOfLists(list(set(listOfTuples(l))))

def edge(src, dest, graph):
  return filter(lambda d : d[0] == dest, graph.edges[src])[0]

def pathLength(path, graph):
  if path == None:
    return { 'total': 0, 'outdoor': 0 }
  total = 0
  outdoor = 0
  src = path[0]
  for dest in path[1:]:
    e = edge(src, dest, graph)
    total += e[1][0]
    outdoor += e[1][1]
    src = dest
  return { 'total': total, 'outdoor': outdoor }

def totalLength(path, graph):
  return pathLength(path, graph)['total']

def pathShortEnough(partialPath, digraph, maxTotalDist, maxDistOutdoors):
  lengthData = pathLength(partialPath, digraph)
  total = lengthData['total']
  outdoor = lengthData['outdoor']
  return total <= maxTotalDist and outdoor <= maxDistOutdoors

def DFS(digraph, start, end, maxTotalDist, maxDistOutdoors, partialPath = [], allPaths = []):
  partialPath = partialPath + [start]
  if start == end and not partialPath in allPaths and (pathShortEnough(partialPath, digraph, maxTotalDist, maxDistOutdoors)):
    allPaths.append(partialPath)
    return allPaths
  children = digraph.childrenOf(start)
  for ch in children:
    if not ch in partialPath:
      e = edge(start, ch, digraph)
      a = DFS(digraph, ch, end, maxTotalDist, maxDistOutdoors, partialPath, allPaths)
      allPaths = uniq(allPaths + a)
  return allPaths

def bruteForceSearch(digraph, start, end, maxTotalDist, maxDistOutdoors):
  paths = DFS(digraph, Node(start), Node(end), maxTotalDist, maxDistOutdoors, [], [])
  if paths == []:
    raise ValueError
  return map(lambda n : str(n), reduce(lambda init, p : init if (totalLength(init, digraph) <= totalLength(p, digraph)) else p, paths))

#
# Problem 4: Finding the Shorest Path using Optimized Search Method
#

def shortDFS(digraph, start, end, maxTotalDist, maxDistOutdoors, partialPath = [], shortestPath = None):
  partialPath = partialPath + [start]
  if start == end and (shortestPath == None or totalLength(partialPath, digraph) < totalLength(shortestPath, digraph)) and pathShortEnough(partialPath, digraph, maxTotalDist, maxDistOutdoors):
    return partialPath
  children = digraph.childrenOf(start)
  for ch in children:
    if not ch in partialPath:
      if shortestPath == None or totalLength(partialPath, digraph) < totalLength(shortestPath, digraph):
        newPath = shortDFS(digraph, ch, end, maxTotalDist, maxDistOutdoors, partialPath, shortestPath)
        if newPath != None:
          shortestPath = newPath
  return shortestPath

def directedDFS(digraph, start, end, maxTotalDist, maxDistOutdoors):
  path = shortDFS(digraph, Node(start), Node(end), maxTotalDist, maxDistOutdoors, [], None)
  if path == None:
    raise ValueError
  return map(lambda n : str(n), path)

# Uncomment below when ready to test
### NOTE! These tests may take a few minutes to run!! ####
# if __name__ == '__main__':
#   # Test cases
#   mitMap = load_map("mit_map.txt")
#   print isinstance(mitMap, Digraph)
#   print isinstance(mitMap, WeightedDigraph)
#   print 'nodes', mitMap.nodes
#   print 'edges', mitMap.edges


#   LARGE_DIST = 1000000

#   # Test case 1
#   print "---------------"
#   print "Test case 1:"
#   print "Find the shortest-path from Building 32 to 56"
#   expectedPath1 = ['32', '56']
#   brutePath1 = bruteForceSearch(mitMap, '32', '56', LARGE_DIST, LARGE_DIST)
#   # dfsPath1 = directedDFS(mitMap, '32', '56', LARGE_DIST, LARGE_DIST)
#   print "Expected: ", expectedPath1
#   print "Brute-force: ", brutePath1
  # print "DFS: ", dfsPath1
  # print "Correct? BFS: {0}; DFS: {1}".format(expectedPath1 == brutePath1, expectedPath1 == dfsPath1)

  # # Test case 2
  # print "---------------"
  # print "Test case 2:"
  # print "Find the shortest-path from Building 32 to 56 without going outdoors"
  # expectedPath2 = ['32', '36', '26', '16', '56']
  # brutePath2 = bruteForceSearch(mitMap, '32', '56', LARGE_DIST, 0)
  # dfsPath2 = directedDFS(mitMap, '32', '56', LARGE_DIST, 0)
  # print "Expected: ", expectedPath2
  # print "Brute-force: ", brutePath2
  # print "DFS: ", dfsPath2
  # print "Correct? BFS: {0}; DFS: {1}".format(expectedPath2 == brutePath2, expectedPath2 == dfsPath2)

  # # Test case 3
  # print "---------------"
  # print "Test case 3:"
  # print "Find the shortest-path from Building 2 to 9"
  # expectedPath3 = ['2', '3', '7', '9']
  # brutePath3 = bruteForceSearch(mitMap, '2', '9', LARGE_DIST, LARGE_DIST)
  # dfsPath3 = directedDFS(mitMap, '2', '9', LARGE_DIST, LARGE_DIST)
  # print "Expected: ", expectedPath3
  # print "Brute-force: ", brutePath3
  # print "DFS: ", dfsPath3
  # print "Correct? BFS: {0}; DFS: {1}".format(expectedPath3 == brutePath3, expectedPath3 == dfsPath3)

  # # Test case 4
  # print "---------------"
  # print "Test case 4:"
  # print "Find the shortest-path from Building 2 to 9 without going outdoors"
  # expectedPath4 = ['2', '4', '10', '13', '9']
  # brutePath4 = bruteForceSearch(mitMap, '2', '9', LARGE_DIST, 0)
  # dfsPath4 = directedDFS(mitMap, '2', '9', LARGE_DIST, 0)
  # print "Expected: ", expectedPath4
  # print "Brute-force: ", brutePath4
  # print "DFS: ", dfsPath4
  # print "Correct? BFS: {0}; DFS: {1}".format(expectedPath4 == brutePath4, expectedPath4 == dfsPath4)

  # # Test case 5
  # print "---------------"
  # print "Test case 5:"
  # print "Find the shortest-path from Building 1 to 32"
  # expectedPath5 = ['1', '4', '12', '32']
  # brutePath5 = bruteForceSearch(mitMap, '1', '32', LARGE_DIST, LARGE_DIST)
  # dfsPath5 = directedDFS(mitMap, '1', '32', LARGE_DIST, LARGE_DIST)
  # print "Expected: ", expectedPath5
  # print "Brute-force: ", brutePath5
  # print "DFS: ", dfsPath5
  # print "Correct? BFS: {0}; DFS: {1}".format(expectedPath5 == brutePath5, expectedPath5 == dfsPath5)

  # # Test case 6
  # print "---------------"
  # print "Test case 6:"
  # print "Find the shortest-path from Building 1 to 32 without going outdoors"
  # expectedPath6 = ['1', '3', '10', '4', '12', '24', '34', '36', '32']
  # brutePath6 = bruteForceSearch(mitMap, '1', '32', LARGE_DIST, 0)
  # dfsPath6 = directedDFS(mitMap, '1', '32', LARGE_DIST, 0)
  # print "Expected: ", expectedPath6
  # print "Brute-force: ", brutePath6
  # print "DFS: ", dfsPath6
  # print "Correct? BFS: {0}; DFS: {1}".format(expectedPath6 == brutePath6, expectedPath6 == dfsPath6)

  # # Test case 7
  # print "---------------"
  # print "Test case 7:"
  # print "Find the shortest-path from Building 8 to 50 without going outdoors"
  # bruteRaisedErr = 'No'
  # dfsRaisedErr = 'No'
  # try:
  #     bruteForceSearch(mitMap, '8', '50', LARGE_DIST, 0)
  # except ValueError:
  #     bruteRaisedErr = 'Yes'

  # try:
  #     directedDFS(mitMap, '8', '50', LARGE_DIST, 0)
  # except ValueError:
  #     dfsRaisedErr = 'Yes'

  # print "Expected: No such path! Should throw a value error."
  # print "Did brute force search raise an error?", bruteRaisedErr
  # print "Did DFS search raise an error?", dfsRaisedErr

  # # Test case 8
  # print "---------------"
  # print "Test case 8:"
  # print "Find the shortest-path from Building 10 to 32 without walking"
  # print "more than 100 meters in total"
  # bruteRaisedErr = 'No'
  # dfsRaisedErr = 'No'
  # try:
  #     bruteForceSearch(mitMap, '10', '32', 100, LARGE_DIST)
  # except ValueError:
  #     bruteRaisedErr = 'Yes'

  # try:
  #     directedDFS(mitMap, '10', '32', 100, LARGE_DIST)
  # except ValueError:
  #     dfsRaisedErr = 'Yes'

  # print "Expected: No such path! Should throw a value error."
  # print "Did brute force search raise an error?", bruteRaisedErr
  # print "Did DFS search raise an error?", dfsRaisedErr
