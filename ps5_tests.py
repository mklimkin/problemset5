from ps5 import *

# function test
def testF(f, expectedResult, *args):
  try:
    try:
      result = f(*args)
    except expectedResult as er:
      print '.',
      return
    if result == expectedResult:
      print '.',
      return
    print
    print 'F',
    error = "Expected " + str(expectedResult) + ", got " + str(result)
    raise AssertionError(error)
  except AssertionError as er:
    print '- ' + str(er)

# brute force tests
def bruteForceTest(expectedResult, *args):
  testF(bruteForceSearch, expectedResult, *args)

# # DFS tests
# def DFSTest(expectedResult, *args):
#   testF(DFS, expectedResult, *args)

# directedDFS tests
def directedDFSTest(expectedResult, *args):
  testF(directedDFS, expectedResult, *args)


# test cases
n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
# DFSTest([[n1]], g, n1, n1, 2, 2, [], [])
bruteForceTest(['1'], g, '1', '1', 2, 2)
directedDFSTest(['1'], g, '1', '1', 2, 2)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
# DFSTest([], g, n1, n2, 2, 2, [], [])
bruteForceTest(ValueError, g, '1', '2', 2, 2)
directedDFSTest(ValueError, g, '1', '2', 2, 2)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 1, 1))
# DFSTest([[n1, n2]], g, n1, n2, 4, 4, [], [])
bruteForceTest(['1', '2'], g, '1', '2', 4, 4)
directedDFSTest(['1', '2'], g, '1', '2', 4, 4)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 5, 5))
# DFSTest([], g, n1, n2, 4, 4, [], [])
bruteForceTest(ValueError, g, '1', '2', 4, 4)
directedDFSTest(ValueError, g, '1', '2', 4, 4)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 1, 1))
n3 = Node('3')
g.addNode(n3)
g.addEdge(WeightedEdge(n1, n3, 1, 1))
# DFSTest([[n1, n3]], g, n1, n3, 2, 2, [], [])
# DFSTest([[n1, n2]], g, n1, n2, 2, 2, [], [])
bruteForceTest(['1', '3'], g, '1', '3', 2, 2)
bruteForceTest(['1', '2'], g, '1', '2', 2, 2)
directedDFSTest(['1', '3'], g, '1', '3', 2, 2)
directedDFSTest(['1', '2'], g, '1', '2', 2, 2)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 1, 1))
n3 = Node('3')
g.addNode(n3)
g.addEdge(WeightedEdge(n1, n3, 1, 1))
g.addEdge(WeightedEdge(n1, n2, 1, 1))
g.addEdge(WeightedEdge(n2, n3, 1, 1))
# DFSTest([[n1, n3]], g, n1, n3, 1, 1, [], [])
bruteForceTest(['1', '3'], g, '1', '3', 1, 1)
directedDFSTest(['1', '3'], g, '1', '3', 1, 1)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 1, 1))
n3 = Node('3')
g.addNode(n3)
g.addEdge(WeightedEdge(n1, n3, 1, 1))
g.addEdge(WeightedEdge(n1, n2, 1, 1))
g.addEdge(WeightedEdge(n2, n3, 1, 1))
n4 = Node('4')
g.addNode(n4)
# DFSTest([], g, n1, n4, 2, 2, [], [])
bruteForceTest(ValueError, g, '1', '4', 2, 2)
directedDFSTest(ValueError, g, '1', '4', 2, 2)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 1, 1))
n3 = Node('3')
g.addNode(n3)
g.addEdge(WeightedEdge(n1, n3, 1, 1))
g.addEdge(WeightedEdge(n1, n2, 1, 1))
g.addEdge(WeightedEdge(n2, n3, 1, 1))
n4 = Node('4')
g.addNode(n4)
g.addEdge(WeightedEdge(n2, n4, 1, 1))
g.addEdge(WeightedEdge(n4, n3, 1, 1))
# DFSTest([[n1, n2, n3], [n1, n3], [n1, n2, n4, n3]], g, n1, n3, 3, 3, [], [])
bruteForceTest(['1', '3'], g, '1', '3', 3, 3)
bruteForceTest(['1', '3'], g, '1', '3', 1, 1)
directedDFSTest(['1', '3'], g, '1', '3', 3, 3)
directedDFSTest(['1', '3'], g, '1', '3', 1, 1)

n1 = Node('1')
g = WeightedDigraph()
g.addNode(n1)
n2 = Node('2')
g.addNode(n2)
g.addEdge(WeightedEdge(n1, n2, 1, 1))
n3 = Node('3')
g.addNode(n3)
g.addEdge(WeightedEdge(n1, n3, 4, 4))
g.addEdge(WeightedEdge(n1, n2, 1, 1))
g.addEdge(WeightedEdge(n2, n3, 1, 1))
n4 = Node('4')
g.addNode(n4)
g.addEdge(WeightedEdge(n2, n4, 1, 1))
g.addEdge(WeightedEdge(n4, n3, 1, 1))
# DFSTest([[n1, n2, n3], [n1, n2, n4, n3]], g, n1, n3, 3, 3, [], [])
bruteForceTest(['1', '2', '3'], g, '1', '3', 2, 2)
bruteForceTest(ValueError, g, '1', '3', 1, 1)
directedDFSTest(['1', '2', '3'], g, '1', '3', 2, 2)
directedDFSTest(ValueError, g, '1', '3', 1, 1)

# 1->2 (10.0, 5.0)
# 1->4 (5.0, 1.0)
# 2->3 (8.0, 5.0)
# 4->3 (8.0, 5.0)
g = WeightedDigraph()
n1, n2, n3, n4 = Node('1'), Node('2'), Node('3'), Node('4')
g.addNode(n1)
g.addNode(n2)
g.addNode(n3)
g.addNode(n4)
g.addEdge(WeightedEdge(n1, n2, 10.0, 5.0))
g.addEdge(WeightedEdge(n1, n4, 5.0, 1.0))
g.addEdge(WeightedEdge(n2, n3, 8.0, 5.0))
g.addEdge(WeightedEdge(n4, n3, 8.0, 5.0))
bruteForceTest(['1', '4', '3'], g, '1', '3', 18, 18)
directedDFSTest(['1', '4', '3'], g, '1', '3', 18, 18)

# 1->2 (5.0, 2.0)
# 3->5 (6.0, 3.0)
# 2->3 (20.0, 10.0)
# 2->4 (10.0, 5.0)
# 4->3 (2.0, 1.0)
# 4->5 (20.0, 10.0)
g = WeightedDigraph()
n1, n2, n3, n4, n5 = Node('1'), Node('2'), Node('3'), Node('4'), Node('5')
g.addNode(n1)
g.addNode(n2)
g.addNode(n3)
g.addNode(n4)
g.addNode(n5)
g.addEdge(WeightedEdge(n1, n2, 5.0, 2.0))
g.addEdge(WeightedEdge(n3, n5, 6.0, 3.0))
g.addEdge(WeightedEdge(n2, n3, 20.0, 10.0))
g.addEdge(WeightedEdge(n2, n4, 10.0, 5.0))
g.addEdge(WeightedEdge(n4, n3, 2.0, 1.0))
g.addEdge(WeightedEdge(n4, n5, 20.0, 10.0))
bruteForceTest(['4', '3', '5'], g, '4', '5', 21, 11)
directedDFSTest(['4', '3', '5'], g, '4', '5', 21, 11)

# 1->2 (5.0, 2.0)
# 3->5 (5.0, 1.0)
# 2->3 (20.0, 10.0)
# 2->4 (10.0, 5.0)
# 4->3 (5.0, 1.0)
# 4->5 (20.0, 1.0)
g = WeightedDigraph()
n1, n2, n3, n4, n5 = Node('1'), Node('2'), Node('3'), Node('4'), Node('5')
g.addNode(n1)
g.addNode(n2)
g.addNode(n3)
g.addNode(n4)
g.addNode(n5)
g.addEdge(WeightedEdge(n1, n2, 5.0, 2.0))
g.addEdge(WeightedEdge(n3, n5, 5.0, 1.0))
g.addEdge(WeightedEdge(n2, n3, 20.0, 10.0))
g.addEdge(WeightedEdge(n2, n4, 10.0, 5.0))
g.addEdge(WeightedEdge(n4, n3, 5.0, 1.0))
g.addEdge(WeightedEdge(n4, n5, 20.0, 1.0))
bruteForceTest(['4', '3', '5'], g, '4', '5', 21, 11)
directedDFSTest(['4', '3', '5'], g, '4', '5', 21, 11)



